package de.exb.interviews.sousa.exceptions;

public class InvalidTokenException extends BusinessException {
	private static final long serialVersionUID = 7695114356931668635L;

	public InvalidTokenException(String message) {
		super(message);
	}
}
