package de.exb.interviews.sousa.auth;

import io.dropwizard.auth.Authorizer;
import de.exb.interviews.sousa.vo.UserVO;

public class ApiAuthorizer implements Authorizer<UserVO> {

	@Override
	public boolean authorize(UserVO user, String role) {
		switch (role) {
		case "ADMIN":
			return user.isAdmin();
		}
		//Default for other roles is true
		return true;
	}

}
