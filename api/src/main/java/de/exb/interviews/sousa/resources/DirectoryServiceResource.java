package de.exb.interviews.sousa.resources;

import io.dropwizard.auth.Auth;

import java.security.Principal;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.exb.interviews.sousa.service.FileServerService;
import de.exb.interviews.sousa.vo.FileVO;

@Path("/api/v1/directories")
@Produces("application/json")
public class DirectoryServiceResource {
	private FileServerService service;

	@Inject
	public DirectoryServiceResource(FileServerService service) {
		super();
		this.service = service;
	}

	@GET
	public List<FileVO> listFilesUser(@Auth Principal user) {
		return service.listFiles(user);
	}

	@POST
	@RolesAllowed("ADMIN")
	public Response createDirectory(@Auth Principal user, @QueryParam("path") String path) {
		service.createDirectory(user, path);
		return Response.status(Status.CREATED).build();
	}
}
