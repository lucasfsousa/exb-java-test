package de.exb.interviews.sousa.service;

import static org.jose4j.jws.AlgorithmIdentifiers.HMAC_SHA256;

import javax.inject.Inject;

import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;

import de.exb.interviews.sousa.configuration.JwtConfiguration;
import de.exb.interviews.sousa.exceptions.BusinessException;
import de.exb.interviews.sousa.exceptions.InvalidAuthenticationException;
import de.exb.interviews.sousa.vo.UserVO;

public class LoginServiceImpl implements LoginService{
	private JwtConfiguration jwtConfiguration;
	
	@Inject
	public LoginServiceImpl(JwtConfiguration jwtConfiguration) {
		super();
		this.jwtConfiguration = jwtConfiguration;
	}

	@Override
	public UserVO login(String username, String password) {
		//Mock method, the correct way is authenticated in some source
		if(username.equals("user") && password.equals("user")){
			return new UserVO(username, false);
		}
		else if(username.equals("admin") && password.equals("admin")){
			return new UserVO(username, true);
		}
		else{
			throw new InvalidAuthenticationException("Invalid username/password");
		}
	}

	@Override
	public String generateToken(UserVO user) {
		final JwtClaims claims = new JwtClaims();
        claims.setClaim("username", user.getUsername());
        claims.setClaim("admin", user.isAdmin());
		
		final JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(HMAC_SHA256);
        jws.setKey(new HmacKey(jwtConfiguration.getSecret().getBytes()));
		
        try {
			return jws.getCompactSerialization();
		} catch (JoseException e) {
			throw new BusinessException("Error generating token");
		}
	}
}
