package de.exb.interviews.sousa.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.never;

import java.net.URL;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import de.exb.interviews.sousa.builder.FileVOBuilder;
import de.exb.interviews.sousa.exceptions.BusinessException;
import de.exb.interviews.sousa.service.FileService;

public class DirectoryServiceResourceTest {
	@InjectMocks
	private DirectoryServiceResource resource;
	
	@Mock
	private FileVOBuilder fileVOBuilder;
	
	@Mock
	private FileService fileService;
	
	public DirectoryServiceResourceTest() {
		MockitoAnnotations.initMocks(this);
		resource = new DirectoryServiceResource(fileService, fileVOBuilder);
	}
	
	@Test
	public void shouldThrowExceptionDeleteInvalidDirectory() throws Exception{
		String path = "abc";
		boolean recursive = false;
		
		when(fileService.isDirectory(any(URL.class))).thenReturn(false);
		
		try {
			resource.deleteDirectory(path, recursive);
			fail("It shouldnt works delete invalid directory");
		} catch (BusinessException e) {
			assertEquals("Invalid directory.", e.getMessage());
		}
		
		verify(fileService).isDirectory(any(URL.class));
		verifyNoMoreInteractions(fileService);
	}
	
	@Test
	public void shouldThrowExceptionDeleteDirectoryNotEmpty() throws Exception{
		String path = "abc";
		boolean recursive = false;
		
		when(fileService.isDirectory(any(URL.class))).thenReturn(true);
		when(fileService.hasChildren(any(URL.class))).thenReturn(true);
		
		try {
			resource.deleteDirectory(path, recursive);
			fail("It shouldnt works delete not empty directory");
		} catch (BusinessException e) {
			assertEquals("Directory is not empty, use recursive=true", e.getMessage());
		}
		
		verify(fileService).isDirectory(any(URL.class));
		verify(fileService).hasChildren(any(URL.class));
		verifyNoMoreInteractions(fileService);
	}
	
	@Test
	public void shouldDeleteDirectoryEmpty() throws Exception{
		String path = "abc";
		boolean recursive = false;
		
		when(fileService.isDirectory(any(URL.class))).thenReturn(true);
		when(fileService.hasChildren(any(URL.class))).thenReturn(false);
		
		Response response = resource.deleteDirectory(path, recursive);
		
		assertEquals(Status.NO_CONTENT.getStatusCode(), response.getStatus());
		
		verify(fileService).isDirectory(any(URL.class));
		verify(fileService).hasChildren(any(URL.class));
		verify(fileService).delete(any(URL.class), eq(recursive));
		verifyNoMoreInteractions(fileService);
	}
	
	@Test
	public void shouldDeleteDirectoryNotEmptyRecursive() throws Exception{
		String path = "abc";
		boolean recursive = true;
		
		when(fileService.isDirectory(any(URL.class))).thenReturn(true);
		
		Response response = resource.deleteDirectory(path, recursive);
		
		assertEquals(Status.NO_CONTENT.getStatusCode(), response.getStatus());
		
		verify(fileService).isDirectory(any(URL.class));
		verify(fileService, never()).hasChildren(any(URL.class)); //If is recursive, never verify children
		verify(fileService).delete(any(URL.class), eq(recursive));
		verifyNoMoreInteractions(fileService);
	}
}
