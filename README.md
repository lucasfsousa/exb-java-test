#ExB Java Test#
This is a test for ExB company, the main idea is to create a REST service to manipulate files in a storage.

#Server#
Responsible for I/O operations, don't have access control, because of this, it's not a good to have external access.

The communication is using REST API, described below.

Endpoint prefix: */api/v1*

**Directories:**

 - GET /directories?path=[path]

 List files/directories in a given path

 - POST /directories?path=[path]

 Create structure of directories in a given path

 - DELETE /directories?path=[path]&recursive=[recursive]

 Delete a given path, if the path is not empty, you must use the parameter *recursive=true*
 
 **Files:**

 - GET /files?path=[path]

 View the content of a file in a given path

 - POST /files?path=[path]

 Create a file named by the path, the binary of the file you have to pass in a request part named *content*

 - DELETE /files?path=[path]

 Delete a file in a given path

#API#

 Responsible for external communication, has a simple authentication using [JWT](https://jwt.io/).

Control user's files, every user has a unique directory, like the *"home"* of UNIX systems.

Endpoint prefix: */api/v1*

**Login:**

 - POST /login

You have to inform two headers: *username* and *password*

Service used to receive a valid token, required for the other operations.

We are using a mock authentication, so we have only two users, *user* and *admin*, the password is the same as the username.

**Using JWT Authentication:**

In all the operations below, have to be used a valid JWT token in the Authorization header, to generate a valid token use the Login Endpoint:

> Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiYWRtaW4iOnRydWV9._l2oFUiJJzYjBeADQtXMFw7TSCLKFkQSxBOhcfrQX_U

**Directories**

- GET /directories

 List user's files/directories

 - POST /directories?path=[path]

 Create structure of directories inside the user's *"home directory"*

 This operation requires that the user has admin role

**Files**

 - POST /files?path=[path]

 Create a file inside the user's *"home directory"* named by the path.

 The binary of the file you have to pass in a request part named *content*

#TODO#

 - Automated tests
 - Web client
 - Apache Camel client
 - Documentation using Swagger
 - Finish the API with the others endpoints

 
Developed by: Lucas Felix de Sousa - [lucasfsousa@gmail.com](mailto:lucasfsousa@gmail.com)