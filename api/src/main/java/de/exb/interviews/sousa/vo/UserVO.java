package de.exb.interviews.sousa.vo;

import java.security.Principal;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserVO implements Principal {
	private String username;
	private boolean admin;

	public UserVO(String username, boolean admin) {
		super();
		this.username = username;
		this.admin = admin;
	}

	public UserVO() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	@JsonIgnore
	public String getName() {
		return username;
	}

}
