package de.exb.interviews.sousa.resources;

import io.dropwizard.auth.Auth;

import java.security.Principal;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import de.exb.interviews.sousa.service.LoginService;
import de.exb.interviews.sousa.vo.TokenVO;
import de.exb.interviews.sousa.vo.UserVO;

@Path("/api/v1/login")
@Produces("application/json")
public class LoginServiceResource {
	private LoginService loginService;
	
	@Inject
	public LoginServiceResource(LoginService loginService) {
		super();
		this.loginService = loginService;
	}

	@POST
	public TokenVO login(@NotNull @HeaderParam("username") String username, @NotNull @HeaderParam("password") String password){
		UserVO user = loginService.login(username, password);
		String token = loginService.generateToken(user);
		return new TokenVO(token, user);
	}
	
	@GET
	public UserVO userData(@Auth Principal user){
		UserVO userVO = (UserVO) user;
		return userVO;
	}
}
