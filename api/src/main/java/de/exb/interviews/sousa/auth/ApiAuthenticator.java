package de.exb.interviews.sousa.auth;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;

import java.util.Optional;

import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.JwtContext;

import de.exb.interviews.sousa.exceptions.InvalidTokenException;
import de.exb.interviews.sousa.vo.UserVO;

public class ApiAuthenticator implements Authenticator<JwtContext, UserVO> {
	@Override
	public Optional<UserVO> authenticate(JwtContext context) throws AuthenticationException {
		try {
			String username = context.getJwtClaims().getClaimValue("username", String.class);
			Boolean admin = context.getJwtClaims().getClaimValue("admin", Boolean.class);
			return Optional.of(new UserVO(username, admin));
		} catch (MalformedClaimException e) {
			throw new InvalidTokenException("Invalid token.");
		}
	}

}
