package de.exb.interviews.sousa.service;

import java.io.InputStream;
import java.security.Principal;
import java.util.List;

import de.exb.interviews.sousa.vo.FileVO;

public interface FileServerService {
	public List<FileVO> listFiles(Principal user);
	public void createDirectory(Principal user, String path);
	public void createFile(Principal user, String path, InputStream content);
}
