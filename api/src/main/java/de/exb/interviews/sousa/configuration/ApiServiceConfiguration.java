package de.exb.interviews.sousa.configuration;

import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ApiServiceConfiguration extends Configuration {
	@NotNull
	@Valid
	private JwtConfiguration jwt;
	
	@NotNull
	@Valid
	private FileServerConfiguration fileServer;

	public JwtConfiguration getJwt() {
		return jwt;
	}

	public void setJwt(JwtConfiguration jwt) {
		this.jwt = jwt;
	}

	public FileServerConfiguration getFileServer() {
		return fileServer;
	}

	public void setFileServer(FileServerConfiguration fileServer) {
		this.fileServer = fileServer;
	}
	
	
}
