package de.exb.interviews.sousa.exceptions;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import de.exb.interviews.sousa.vo.ErrorVO;

@Provider
public class GlobalErrorHandler implements ExceptionMapper<Exception> {
	@Override
	public Response toResponse(Exception e) {
		e.printStackTrace();
		
		int status = 500;
		Object entity = new ErrorVO("Internal server error", e.getMessage());
		
		if (e instanceof IntegrationException){
			return ((IntegrationException) e).getResponse();
		}
		else if (e instanceof InvalidAuthenticationException){
			status = 401;
			entity = new ErrorVO(e.getMessage());
		}
		else if(e instanceof InvalidTokenException){
			status = 401;
			entity = new ErrorVO(e.getMessage());
		}
		else if (e instanceof ForbiddenException){
			status = 403;
			entity = new ErrorVO("Not authorized");
		}
		else if(e instanceof BusinessException){
			status = 400;
			entity = new ErrorVO(e.getMessage());
		}
		
		return Response.status(status).entity(entity).type("application/json").build();
	}
}
