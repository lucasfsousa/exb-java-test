package de.exb.interviews.sousa.exceptions;

public class FileNotExistsException extends BusinessException {
	private static final long serialVersionUID = 2227967993906681200L;

	public FileNotExistsException(String message) {
		super(message);
	}

}
