package de.exb.interviews.sousa.exceptions;

public class InvalidAuthenticationException extends BusinessException {
	private static final long serialVersionUID = 2428648622367927162L;

	public InvalidAuthenticationException(String message) {
		super(message);
	}

}
