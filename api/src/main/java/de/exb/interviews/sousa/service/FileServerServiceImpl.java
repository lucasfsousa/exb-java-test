package de.exb.interviews.sousa.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.google.common.io.ByteStreams;

import de.exb.interviews.sousa.configuration.FileServerConfiguration;
import de.exb.interviews.sousa.exceptions.BusinessException;
import de.exb.interviews.sousa.exceptions.IntegrationException;
import de.exb.interviews.sousa.vo.FileVO;

public class FileServerServiceImpl implements FileServerService {
	private FileServerConfiguration fileServerConfiguration;
	
	@Inject
	public FileServerServiceImpl(FileServerConfiguration fileServerConfiguration) {
		super();
		this.fileServerConfiguration = fileServerConfiguration;
	}

	@Override
	public List<FileVO> listFiles(Principal user) {
		String url = fileServerConfiguration.getApiUrl() + "/v1/directories?path=" + getUserBasePath(user);;

		Response response = ClientBuilder.newClient().target(url).request().get();
		
		verifyResponse(response);
		
		return (List<FileVO>) response.readEntity(List.class);
	}

	@Override
	public void createDirectory(Principal user, String path) {
		String directoryPath = getUserBasePath(user) + "/" + path;
		String url = fileServerConfiguration.getApiUrl() + "/v1/directories?path=" + directoryPath;
		Response response = ClientBuilder.newClient().target(url).request().post(null);
		verifyResponse(response);
	}
	
	@Override
	public void createFile(Principal user, String path, InputStream content) {
		String filePath = getUserBasePath(user) + "/" + path;
		String url = fileServerConfiguration.getApiUrl() + "/v1/files?path=" + filePath;
		
		try {
			/*
			MultiPart multiPartEntity = new MultiPart()
			.bodyPart(ByteStreams.toByteArray(content)
            		, MediaType.APPLICATION_OCTET_STREAM_TYPE);
			*/
			
			FormDataMultiPart formData = new FormDataMultiPart()
				.field("content", ByteStreams.toByteArray(content), MediaType.APPLICATION_OCTET_STREAM_TYPE);
			
			Response response = ClientBuilder.newBuilder()
					.register(MultiPartFeature.class).build()
					.target(url)
					.request()
					.post(Entity.entity(formData, formData.getMediaType()));
			
			verifyResponse(response);
		} catch (IOException e) {
			throw new BusinessException("Error creating file");
		}
	}
	
	private String getUserBasePath(Principal user){
		return fileServerConfiguration.getBasePath() + "/" + user.getName();
	}
	
	private void verifyResponse(Response response){
		if(response.getStatus() >= 300){
			throw new IntegrationException(response);
		}
	}
}
