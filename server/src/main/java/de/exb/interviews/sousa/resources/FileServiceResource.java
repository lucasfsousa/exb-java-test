package de.exb.interviews.sousa.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.common.io.ByteStreams;

import de.exb.interviews.sousa.exceptions.BusinessException;
import de.exb.interviews.sousa.exceptions.FileServiceException;
import de.exb.interviews.sousa.service.FileService;
import de.exb.interviews.sousa.utils.Utils;

@Path("/api/v1/files")
@Produces("application/json")
public class FileServiceResource {
	private final FileService fileService;
	
	@Inject
	public FileServiceResource(FileService fileService) {
		this.fileService = fileService;
	}
	
	@GET
	@Produces("application/octet-stream")
	public Response viewFile(@NotNull @QueryParam("path") String path) throws FileServiceException{
		URL url = Utils.getUrl(path);
		InputStream inputStream = fileService.openForReading(url);
		return Response.ok(inputStream).build();
	}
	
	@POST
	public Response createFile(@NotNull @QueryParam("path") String path, @FormDataParam("content") InputStream content) throws IOException{
		URL url = Utils.getUrl(path);
		
		//create a new file
		fileService.createNewFile(url);
		
		//open file and write
		OutputStream outputStream = fileService.openForWriting(url, false);
		//convert inputstream no byte[] using Google IO library
		outputStream.write(ByteStreams.toByteArray(content));
		
		return Response.status(Status.CREATED).build();
	}
	
	@DELETE
	public Response deleteFile(@NotNull @QueryParam("path") String path) throws IOException{
		URL url = Utils.getUrl(path);
		if(!fileService.isFile(url)){
			throw new BusinessException("Invalid file.");
		}
		fileService.delete(url, false);
		return Response.status(Status.NO_CONTENT).build();
	}
}
