package de.exb.interviews.sousa.exceptions;

public class URLIsNotDirectoryException extends BusinessException {
	private static final long serialVersionUID = -8294131808046332528L;

	public URLIsNotDirectoryException(String message) {
		super(message);
	}

}
