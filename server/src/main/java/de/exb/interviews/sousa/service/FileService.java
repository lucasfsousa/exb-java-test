package de.exb.interviews.sousa.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import javax.validation.constraints.NotNull;

import de.exb.interviews.sousa.exceptions.FileServiceException;

public interface FileService {

	@NotNull
	public OutputStream openForWriting(@NotNull final URL aPath, final boolean aAppend) throws FileServiceException;

	@NotNull
	public InputStream openForReading(@NotNull final URL aPath)	throws FileServiceException;

	@NotNull
	public URL construct(@NotNull final String aPath) throws FileServiceException;

	@NotNull
	public URL construct(@NotNull final URL aParentPath, @NotNull final String aChildPath) throws FileServiceException;

	public void createNewFile(@NotNull final URL aPath) throws FileServiceException;

	public void mkdir(@NotNull final URL aPath) throws FileServiceException;

	public void mkdirs(@NotNull final URL aPath) throws FileServiceException;

	@NotNull
	public List<URL> list(@NotNull final URL aPath) throws FileServiceException;

	public void delete(@NotNull final URL aPath, final boolean aRecursive) throws FileServiceException;

	public boolean exists(@NotNull final URL aPath) throws FileServiceException;

	public boolean isFile(@NotNull final URL aPath) throws FileServiceException;

	public boolean isDirectory(@NotNull final URL aPath) throws FileServiceException;
	
	public boolean hasChildren(@NotNull final URL aPath) throws FileServiceException;

	public long getSize(@NotNull final URL aPath) throws FileServiceException;

	@NotNull
	public URL getParent(@NotNull final URL aPath) throws FileServiceException;

	@NotNull
	public String getName(@NotNull final URL aPath) throws FileServiceException;
}
