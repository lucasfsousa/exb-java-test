package de.exb.interviews.sousa.utils;

import java.net.MalformedURLException;
import java.net.URL;

import de.exb.interviews.sousa.exceptions.BusinessException;

public class Utils {
	private static final String FILE_PREFIX = "file:";
	
	public static URL getUrl(String path) {
		try {
			return new URL(FILE_PREFIX + path);
		} catch (MalformedURLException e) {
			throw new BusinessException("Error parsing URL from PATH"); 
		}
	}
}
