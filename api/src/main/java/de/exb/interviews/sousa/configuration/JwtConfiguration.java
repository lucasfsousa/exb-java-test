package de.exb.interviews.sousa.configuration;

import javax.validation.constraints.NotNull;

public class JwtConfiguration {
	@NotNull
	private String secret;

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

}
