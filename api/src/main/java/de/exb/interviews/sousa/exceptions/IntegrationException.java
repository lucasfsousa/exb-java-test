package de.exb.interviews.sousa.exceptions;

import javax.ws.rs.core.Response;

public class IntegrationException extends RuntimeException {
	private static final long serialVersionUID = -354866027111250343L;
	private Response response;

	public IntegrationException(Response response) {
		this.response = response;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

}
