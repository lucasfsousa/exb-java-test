package de.exb.interviews.sousa;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import de.exb.interviews.sousa.configuration.CombinedSourceProvider;
import de.exb.interviews.sousa.configuration.FileServiceConfiguration;
import de.exb.interviews.sousa.exceptions.GlobalErrorHandler;
import de.exb.interviews.sousa.resources.DirectoryServiceResource;
import de.exb.interviews.sousa.resources.FileServiceResource;
import de.exb.interviews.sousa.service.FileService;
import de.exb.interviews.sousa.service.FileServiceImpl;

public class FileServiceApplication extends Application<FileServiceConfiguration> {

	public static void main(final String[] aArgs) throws Exception {
		new FileServiceApplication().run(new String[] { "server", "classpath:server.yml" });
	}

	@Override
	public void initialize(final Bootstrap<FileServiceConfiguration> aBootstrap) {
		aBootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
				new CombinedSourceProvider(), new EnvironmentVariableSubstitutor()));
		aBootstrap.addBundle(new MultiPartBundle());
	}

	@Override
	public void run(final FileServiceConfiguration aConfiguration, final Environment aEnvironment) {
		Injector injector = createInjector(aConfiguration);
		
		//Register global error handler
		aEnvironment.jersey().register(injector.getInstance(GlobalErrorHandler.class));
		
		//Register resources
		aEnvironment.jersey().register(injector.getInstance(FileServiceResource.class));
		aEnvironment.jersey().register(injector.getInstance(DirectoryServiceResource.class));
		
	}

	private Injector createInjector(final FileServiceConfiguration conf) {
		return Guice.createInjector(new AbstractModule() {
	        @Override
	        protected void configure() {
	        	//Override default behavior for some classes/interfaces
	            bind(FileServiceConfiguration.class).toInstance(conf);
	            bind(FileService.class).to(FileServiceImpl.class);
	        }
	    });
	}
}
