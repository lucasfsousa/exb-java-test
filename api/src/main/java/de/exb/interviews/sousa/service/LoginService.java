package de.exb.interviews.sousa.service;

import de.exb.interviews.sousa.vo.UserVO;

public interface LoginService {
	public UserVO login(String username, String password);
	public String generateToken(UserVO user);
}
