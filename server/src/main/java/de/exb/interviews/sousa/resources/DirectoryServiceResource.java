package de.exb.interviews.sousa.resources;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.exb.interviews.sousa.builder.FileVOBuilder;
import de.exb.interviews.sousa.exceptions.BusinessException;
import de.exb.interviews.sousa.exceptions.FileServiceException;
import de.exb.interviews.sousa.service.FileService;
import de.exb.interviews.sousa.utils.Utils;
import de.exb.interviews.sousa.vo.FileVO;

@Path("/api/v1/directories")
@Produces("application/json")
public class DirectoryServiceResource {
	private final FileService fileService;
	private final FileVOBuilder fileVOBuilder;
	
	@Inject
	public DirectoryServiceResource(FileService fileService, FileVOBuilder fileVOBuilder) {
		this.fileService = fileService;
		this.fileVOBuilder = fileVOBuilder;
	}
	
	@GET
	public List<FileVO> listFiles(@NotNull @QueryParam("path") String path) throws FileServiceException {
		List<URL> listUrls = fileService.list(Utils.getUrl(path));
		
		//Iterate over each url to create the FileVO object
		List<FileVO> list = new ArrayList<FileVO>();
		for (URL url : listUrls) {
			list.add(fileVOBuilder.withURL(url).build());
		}
		
		return list;
	}
	
	@POST
	public Response createDirectory(@NotNull @QueryParam("path") String path) throws IOException{
		fileService.mkdirs(Utils.getUrl(path));
		return Response.status(Status.CREATED).build();
	}
	
	@DELETE
	public Response deleteDirectory(@NotNull @QueryParam("path") String path, @QueryParam("recursive") boolean recursive) throws IOException{
		URL url = Utils.getUrl(path);
		
		//Verify if it is a directory
		if(!fileService.isDirectory(url)){
			throw new BusinessException("Invalid directory.");
		}
		
		//Verify if recursive is false and the directory is not empty
		if(recursive == false && fileService.hasChildren(url)){
			throw new BusinessException("Directory is not empty, use recursive=true");
		}
		
		fileService.delete(Utils.getUrl(path), recursive);
		return Response.status(Status.NO_CONTENT).build();
	}
}
