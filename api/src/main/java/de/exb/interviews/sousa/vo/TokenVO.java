package de.exb.interviews.sousa.vo;

public class TokenVO {
	private String token;
	private UserVO user;

	public TokenVO(String token, UserVO user) {
		super();
		this.token = token;
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UserVO getUser() {
		return user;
	}

	public void setUser(UserVO user) {
		this.user = user;
	}

}
