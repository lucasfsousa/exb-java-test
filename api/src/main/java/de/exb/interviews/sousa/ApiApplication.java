package de.exb.interviews.sousa;

import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.security.Principal;

import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;

import com.github.toastshaman.dropwizard.auth.jwt.JwtAuthFilter;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import de.exb.interviews.sousa.auth.ApiAuthenticator;
import de.exb.interviews.sousa.auth.ApiAuthorizer;
import de.exb.interviews.sousa.configuration.ApiServiceConfiguration;
import de.exb.interviews.sousa.configuration.CombinedSourceProvider;
import de.exb.interviews.sousa.configuration.FileServerConfiguration;
import de.exb.interviews.sousa.configuration.JwtConfiguration;
import de.exb.interviews.sousa.exceptions.GlobalErrorHandler;
import de.exb.interviews.sousa.resources.DirectoryServiceResource;
import de.exb.interviews.sousa.resources.FileServiceResource;
import de.exb.interviews.sousa.resources.LoginServiceResource;
import de.exb.interviews.sousa.service.FileServerService;
import de.exb.interviews.sousa.service.FileServerServiceImpl;
import de.exb.interviews.sousa.service.LoginService;
import de.exb.interviews.sousa.service.LoginServiceImpl;
import de.exb.interviews.sousa.vo.UserVO;

public class ApiApplication extends Application<ApiServiceConfiguration> {

	public static void main(final String[] aArgs) throws Exception {
		new ApiApplication().run(new String[] { "server", "classpath:server.yml" });
	}

	@Override
	public void initialize(final Bootstrap<ApiServiceConfiguration> aBootstrap) {
		aBootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
				new CombinedSourceProvider(), new EnvironmentVariableSubstitutor()));
		aBootstrap.addBundle(new MultiPartBundle());
	}

	@Override
	public void run(final ApiServiceConfiguration aConfiguration, final Environment aEnvironment) {
		Injector injector = createInjector(aConfiguration);
		
		// Configuration Authentication JWT
		// Based in https://github.com/ToastShaman/dropwizard-auth-jwt
		String secret = aConfiguration.getJwt().getSecret();
		
		final JwtConsumer consumer = new JwtConsumerBuilder()
	        .setVerificationKey(new HmacKey(secret.getBytes()))
	        .build();

		aEnvironment.jersey().register(new AuthDynamicFeature(
	        new JwtAuthFilter.Builder<UserVO>()
	            .setJwtConsumer(consumer)
	            .setRealm("realm")
	            .setPrefix("Bearer")
	            .setAuthenticator(injector.getInstance(ApiAuthenticator.class))
	            .setAuthorizer(new ApiAuthorizer())
	            .buildAuthFilter()));
		
		aEnvironment.jersey().register(new AuthValueFactoryProvider.Binder<>(Principal.class));
		aEnvironment.jersey().register(RolesAllowedDynamicFeature.class);
		
		//End configuration JWT
		
		//Register global error handler
		aEnvironment.jersey().register(injector.getInstance(GlobalErrorHandler.class));
		
		//Register resources
		aEnvironment.jersey().register(injector.getInstance(LoginServiceResource.class));
		aEnvironment.jersey().register(injector.getInstance(DirectoryServiceResource.class));
		aEnvironment.jersey().register(injector.getInstance(FileServiceResource.class));
	}

	private Injector createInjector(final ApiServiceConfiguration conf) {
		return Guice.createInjector(new AbstractModule() {
	        @Override
	        protected void configure() {
	        	//Override default behavior for some classes/interfaces
	            bind(ApiServiceConfiguration.class).toInstance(conf);
	            bind(JwtConfiguration.class).toInstance(conf.getJwt());
	            bind(FileServerConfiguration.class).toInstance(conf.getFileServer());
	            bind(LoginService.class).to(LoginServiceImpl.class);
	            bind(FileServerService.class).to(FileServerServiceImpl.class);
	        }
	    });
	}
}
