package de.exb.interviews.sousa.builder;

import java.net.URL;

import javax.inject.Inject;

import de.exb.interviews.sousa.exceptions.FileServiceException;
import de.exb.interviews.sousa.service.FileService;
import de.exb.interviews.sousa.vo.FileVO;

public class FileVOBuilder {
	private FileService fileService;
	private URL url;
	
	@Inject
	public FileVOBuilder(FileService fileService){
		this.fileService = fileService;
	}
	
	public FileVOBuilder withURL(URL url){
		this.url = url;
		return this;
	}
	
	public FileVO build() throws FileServiceException{
		FileVO file = new FileVO();
		file.setName(fileService.getName(url));
		file.setPath(url.getPath());
		file.setParentPath(fileService.getParent(url).getPath());
		file.setSize(fileService.getSize(url));
		file.setFile(fileService.isFile(url));
		file.setDirectory(fileService.isDirectory(url));
		return file;
	}
}
