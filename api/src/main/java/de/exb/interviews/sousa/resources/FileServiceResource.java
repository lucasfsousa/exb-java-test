package de.exb.interviews.sousa.resources;

import io.dropwizard.auth.Auth;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;

import de.exb.interviews.sousa.service.FileServerService;

@Path("/api/v1/files")
@Produces("application/json")
public class FileServiceResource {
	private FileServerService service;

	@Inject
	public FileServiceResource(FileServerService service) {
		super();
		this.service = service;
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response createFile(@Auth Principal user, @NotNull @QueryParam("path") String path, @FormDataParam("content") InputStream content) throws IOException{
		service.createFile(user, path, content);
		return Response.status(Status.CREATED).build();
	}
}
